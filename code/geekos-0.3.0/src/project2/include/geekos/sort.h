#ifndef SORT_H
#define SORT_H

#include <elf.h>

void copyArray(Elf32_Addr *source, Elf32_Addr *dest, int len, int first);
void merge(Elf32_Addr *a, int left, int right);
void mergeSort(Elf32_Addr *a, int left, int right) ;
extern void MergeSort(Elf32_Addr *a, int n);

#endif