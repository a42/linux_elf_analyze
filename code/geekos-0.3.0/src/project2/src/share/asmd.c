#include <share/asmd.h>

int GK_add(int a, int b)
{
    return (a + b);
}

int GK_sub(int a, int b)
{
    return (a - b);
}

int GK_mul(int a, int b)
{
    return (a * b);
}

int GK_div(int a, int b)
{
    if(0 == a || 0 == b)
        return 0;
    else
        return (a / b);
}
