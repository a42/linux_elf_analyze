#include <conio.h>
#include <share/asmd.h>

int main(int argc, char** argv)
{
	Print("Hi! This is a dynamic test program.\n");
	int a = 1;
	int b = 2;

	Print("a = %d\n", a);
	Print("b = %d\n", b);

	// Print("=============================\n");

	int c1 = GK_add(a, b);
	int c2 = GK_sub(a, b);
	int c3 = GK_mul(a, b);
	int c4 = GK_div(a, b);

	Print("a + b = %d\n", c1);
	Print("a - b = %d\n", c2);
	Print("a * b = %d\n", c3);
	Print("a / b = %d\n", c4);

   	return 0;
}